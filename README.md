# Advanced shell scripting with Bash

Slide decks used at [Catalyst](https://catalyst.net.nz/training/course/advanced-shell-scripting-bash-wellington), [Toitū Te Whenua Land Information New Zealand](https://www.linz.govt.nz/), and [The New Zealand Ministry of Social Development](https://www.msd.govt.nz/). Massive thanks to Catalyst and Toitū Te Whenua Land Information New Zealand for encouragement and allowing the publication of this course!

## Use

1. Go to [the hosted pages](https://engmark.gitlab.io/advanced-shell-scripting-with-bash/)
1. Enjoy!

# Shameless plug

If you find this interesting you can find much more in my book, [The newline Guide to Bash Scripting](https://www.newline.co/courses/newline-guide-to-bash-scripting).

# Copyright

![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")

The content is copyright © 2018 Catalyst IT, 2020 Victor Engmark, and licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). A copy of the license is provided in the [LICENSE](LICENSE) file.

Made with [reveal.js](https://revealjs.com/).
