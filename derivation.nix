{ pkgs }:
let
  pname = "advanced-shell-scripting-with-bash";
  package = pkgs.mkYarnPackage {
    inherit pname;
    version = "1.0.0";

    src = builtins.path {
      name = pname;
      path = ./.;
      filter =
        path: type:
        builtins.elem (/. + path) [
          ./advanced.html
          ./beginner.html
          ./console-screenshot.png
          ./index.html
          ./override.css
          ./package.json
          ./yarn.lock
        ];
    };

    meta = {
      homepage = "https://gitlab.com/engmark/${pname}";
      description = "Advanced shell scripting with Bash course";
      longDescription = builtins.readFile ./README.md;
      maintainers = [ pkgs.lib.maintainers.l0b0 ];
    };
  };
in
pkgs.runCommand pname { } ''
  mkdir --parents "$out"
  tar --directory="$out" --exclude="${pname}" --exclude=package.json --extract --file="${package}"/tarballs/${pname}-${package.version}.tgz --gzip --strip-components=1
  cp --recursive "${package}/libexec/${pname}/node_modules" "$out/"
''
